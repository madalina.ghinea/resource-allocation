from django.db import models

# Create your models here.

from django.db import models
from django.contrib.auth.models import User


class Employee(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    email = models.CharField(max_length=200)
    skill = models.TextField()
    department = models.TextField()
    title = models.CharField(max_length=200, default='none')

    class Meta:
        verbose_name = ("Employee")
        verbose_name_plural = ("Employees")
