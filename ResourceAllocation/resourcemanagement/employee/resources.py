from import_export import resources
from .models import Employee
from projects.models import Projects

class EmployeeResource(resources.ModelResource):
    class Meta:
        model = Employee

