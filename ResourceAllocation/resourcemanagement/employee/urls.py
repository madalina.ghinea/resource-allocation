from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home_page_view, name='home'),
    path('home', views.home_page_view, name='home'),
    path('employee', views.employee_view, name='employee'),
    path('add_employee', views.add_employee, name='add_employee'),
    path('exportEmployee', views.exportEmployee, name='exportEmployee'),
    path('reports', views.reports, name='reports'),
    # path('employeeupload', views.employeeupload, name='employeeupload'),



]
