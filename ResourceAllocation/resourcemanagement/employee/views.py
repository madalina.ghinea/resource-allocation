from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

# Create your views here.

from django.shortcuts import render
from .models import Employee

from django.http import HttpResponse
from .resources import EmployeeResource
from projects.resources import ProjectsResourceImport
from projects.resources import ProjectsResource
from tablib import Dataset

def home_page_view(request):
    return render(request, 'index.html')


def employee_view(request):
    return render(request, 'employee.html')



def add_employee(request):
    if request.method == 'POST':
        if request.POST.get('firstname') and request.POST.get('lastname') and request.POST.get(
                'email') and request.POST.get('skill') and request.POST.get('department') and request.POST.get('title'):
            post = Employee()
            post.firstname = request.POST.get('firstname')
            post.lastname = request.POST.get('lastname')
            post.email = request.POST.get('email')
            post.skill = request.POST.get('skill')
            post.department = request.POST.get('department')
            post.title = request.POST.get('title')
            post.save()

            return render(request, 'add_employee.html')

    else:
        return render(request, 'add_employee.html')


def exportEmployee(request):
    employee_resource = ProjectsResourceImport()
    dataset = employee_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="employee.xls"'
    return response


def reports(request):

    if request.method == 'POST':


        if "employee_import" in request.POST:
            person_resource = EmployeeResource()
            dataset = Dataset()
            new_persons = request.FILES['myfile']

            dataset.load(new_persons.read())
            result = person_resource.import_data(dataset, dry_run=True)  # Test the data import
            print("upload employee")

            if not result.has_errors():
                person_resource.import_data(dataset, dry_run=False)  # Actually import now

        elif "project_import" in request.POST:
            project_resource = ProjectsResourceImport()
            dataset = Dataset()
            new_projects = request.FILES['myfile2']
            # pr_imported_data = dataset.load(new_projects.read())
            dataset.load(new_projects.read())
            result = project_resource.import_data(dataset, dry_run=True)  # Test the data import

            if not result.has_errors():
                project_resource.import_data(dataset, dry_run=False)  # Actually import now
                print("upload projects")
            else:
                print(result)

    return render(request, 'reports.html')

# def employeeupload(request):
#     if request.method == 'POST':
#         person_resource = EmployeeResource()
#         dataset = Dataset()
#         new_persons = request.FILES['myfile']
#
#         dataset.load(new_persons.read())
#         result = person_resource.import_data(dataset, dry_run=True)  # Test the data import
#         print("upload employee")
#
#         if not result.has_errors():
#             person_resource.import_data(dataset, dry_run=False)  # Actually import now
#
#     return render(request, 'reports.html')
