from django.db import models

# Create your models here.


from employee.models import Employee
from django.utils import timezone



class Projects(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    start_date = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
    end_date = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
    employee = models.ForeignKey(Employee, default=None, on_delete=models.SET_NULL, null=True,)
    content = models.TextField(blank=True)

    # class Meta:
    #     ordering = ["-created"]

    def __str__(self):
        return self.name
