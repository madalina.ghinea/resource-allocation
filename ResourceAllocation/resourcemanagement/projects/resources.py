from import_export import resources
from .models import Projects
from import_export import fields, resources, widgets
from import_export.fields import Field

class ProjectsResource(resources.ModelResource):
    class Meta:
        model = Projects
        fields = ('name', 'description', 'start_date', 'end_date', 'employee__firstname', 'employee__lastname')



class ProjectsResourceImport(resources.ModelResource):
    start_date = Field(attribute='start_date', column_name='start_date')
    end_date = Field(attribute='end_date', column_name='end_date')

    class Meta:
        model = Projects
        fields = ('id', 'name', 'description', 'start_date', 'end_date')




