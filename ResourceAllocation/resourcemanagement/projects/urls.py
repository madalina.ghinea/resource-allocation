from django.urls import path, include
from . import views

urlpatterns = [
    path('add_project', views.add_project, name='add_project'),
    path('assignments', views.assignments, name='assignments'),
    path('exportProjects', views.exportProjects, name='exportProjects'),
    # path('uploadprojects', views.uploadprojects, name='uploadprojects'),


]