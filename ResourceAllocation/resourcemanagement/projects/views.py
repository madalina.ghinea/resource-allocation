import datetime
from datetime import date
import numpy as np

from django.shortcuts import render, redirect

# Create your views here.
from employee.models import Employee
from projects.models import Projects
from datetime import timedelta

from django.http import HttpResponse
from .resources import ProjectsResource
from tablib import Dataset


def add_project(request):
    # employees = Employee.objects.all()
    # context = {"employees": employees}

    if request.method == "POST":
        if "Add" in request.POST:
            name = request.POST.get("name")
            description = request.POST.get("description")
            start_date = request.POST.get("start_date")
            end_date = request.POST.get("end_date")
            # employee = request.POST.get("employee_select")
            content = name + " " + description + "-- " + start_date + "--" + end_date
            project = Projects(name=name, description=description, start_date=start_date, end_date=end_date,
                               content=content)
            # employee=Employee.objects.get(id=employee),content=content)

            project.save()
            return redirect("add_project")

    return render(request, "add_project.html")


# def get_working_days(date_start_obj, date_end_obj):
#     start_obj = datetime.datetime.strptime(('date_start_obj'), "%Y-%m-%d").date()
#     end_obj = datetime.datetime.strptime(('date_end_obj'), "%Y-%m-%d").date()
#     weekdays = rrule.rrule(rrule.DAILY, byweekday=range(0, 5), dtstart=start_obj, until=end_obj)
#     weekdays = len(list(weekdays))
#     # if int(time.strftime('%H')) >= 18:
#     #     weekdays -= 1
#     return weekdays


def assignments(request):
    employees = Employee.objects.all()
    context = {"employees": employees}
    projects = Projects.objects.filter(employee_id=None)
    context["projects"] = projects

    if request.method == "POST":
        if request.POST.get("form_type") == 'myForm':
            employee_id = request.POST["employee_select"]

            project_id = request.POST["project_select"]
            project = Projects.objects.get(id=project_id)
            project.employee_id = employee_id
            project.save()
        elif request.POST.get("form_type") == 'WorkloadForm':
            # Handle Elements from second Form
            employee_id = request.POST["employee_select_workload"]

            start_date = datetime.datetime.strptime(request.POST.get('start_date'), "%Y-%m-%d").date()
            end_date = datetime.datetime.strptime(request.POST.get('end_date'), "%Y-%m-%d").date()

            workload_period = start_date - end_date

            projects_workload = Projects.objects.filter(employee_id=employee_id)
            employee_workload = Employee.objects.get(id=employee_id)
            context["projects_workload"] = projects_workload
            context["employee_workload"] = employee_workload
            context["workload_period"] = workload_period.days * (-1)

            context["start_date"] = start_date
            context["end_date"] = end_date

            project_duration_calculated = []

            for project in projects_workload:
                if end_date >= project.start_date:
                    if (start_date <= project.start_date) and (end_date >= project.end_date):
                        duration2 = np.busday_count(project.start_date, project.end_date + datetime.timedelta(days=1))

                        project_duration_calculated.append(duration2)
                    elif end_date >= project.end_date:
                        duration2 = np.busday_count(start_date, project.end_date + datetime.timedelta(days=1))

                        project_duration_calculated.append(duration2)
                    elif end_date <= project.end_date:
                        duration2 = np.busday_count(project.start_date, end_date + datetime.timedelta(days=1))

                        project_duration_calculated.append(duration2)

            context["project_duration_calculated"] = project_duration_calculated

            labels = []
            data = []
            project_start = []
            project_end = []

            for item in project_duration_calculated:
                data.append(item)

            for project in projects_workload:
                labels.append(project.name)
                project_start.append(project.start_date)
                project_end.append(project.end_date)

            project_start.sort()
            project_end.sort()

            project_start_available = []
            project_end_available = []

            for i in range(len(project_start) - 1):
                if project_end[i] < project_start[i + 1]:
                    project_start_available.append(project_end[i] + datetime.timedelta(days=1))
                    project_end_available.append(project_start[i + 1] - datetime.timedelta(days=1))

            if start_date < project_start[0]:
                project_start_available.insert(0, start_date)
                project_end_available.insert(0, project_start[0] - datetime.timedelta(days=1))

            if end_date > project_end[len(project_end) - 1]:
                project_start_available.insert(len(project_start_available), project_end[
                    len(project_end) - 1] + datetime.timedelta(days=1))

                project_end_available.insert(len(project_end_available), end_date)

            total = 0
            for i in range(len(project_start_available)):
                # total += int((project_end_available[i]-project_start_available[i]).days)
                total += np.busday_count(project_start_available[i],
                                         project_end_available[i] + datetime.timedelta(days=1))

            context["project_start_available"] = project_start_available
            context["project_end_available"] = project_end_available

            labels.append("Available days")
            data.append(total)
            context["labels"] = labels
            context["data"] = data

        if request.POST.get("pie") == 'pie':
            context["show_pie"] = "TRUE"

    return render(request, "assignments.html", context)


def exportProjects(request):
    project_resource = ProjectsResource()
    dataset = project_resource.export()
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="project.xls"'
    return response


# def uploadprojects(request):
#     if request.method == 'POST':
#         person_resource = ProjectsResource()
#         dataset = Dataset()
#         new_persons = request.FILES['myfile2']
#
#         dataset.load(new_persons.read())
#         result = person_resource.import_data(dataset, dry_run=True)  # Test the data import
#         print("upload projects")
#
#         if not result.has_errors():
#             person_resource.import_data(dataset, dry_run=False)  # Actually import now
#
#
#     return render(request, 'reports.html')