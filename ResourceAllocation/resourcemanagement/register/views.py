# views.py
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from . import models


from .forms import RegisterForm

# Create your views here.
from django.urls import reverse_lazy

# from ..employee.models import ToDoList


def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()

        return redirect("/home")
    else:
        form = RegisterForm()

    return render(request, "register.html", {"form": form})


def login(request):
    return render(request, 'registration/login.html')



