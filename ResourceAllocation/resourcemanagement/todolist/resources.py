from import_export import resources
from .models import Category, TodoList


class CategoryResource(resources.ModelResource):
    class Meta:
        model = Category


class TodolistResource(resources.ModelResource):
    class Meta:
        model = TodoList
        fields = ('title', 'created' , 'due_date' , 'category__name','employee__firstname', 'employee__lastname')
