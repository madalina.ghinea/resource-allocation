from django.urls import path
from . import views


urlpatterns = [
    path('todolist', views.todolist, name='todolist'),
    path('employee_overview', views.employee_overview, name='employee_overview'),
    path('exportToDoList', views.exportToDoList, name='exportToDoList'),
]