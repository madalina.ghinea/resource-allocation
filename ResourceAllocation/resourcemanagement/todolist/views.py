from django.shortcuts import render

# Create your views here.

from django.shortcuts import render, redirect

from projects.models import Projects
from .models import TodoList, Category
from employee.models import Employee

from django.http import HttpResponse
from .resources import CategoryResource,TodolistResource


def todolist(request):
    todos = TodoList.objects.all()
    categories = Category.objects.all()
    employees = Employee.objects.all()
    if request.method == "POST":
        if "taskAdd" in request.POST:
            title = request.POST["description"]
            date = str(request.POST["date"])
            category = request.POST["category_select"]
            employee = request.POST["employee_select"]
            content = title + " -- " + date + " " + category
            Todo = TodoList(title=title, content=content, due_date=date, category=Category.objects.get(name=category),
                            employee=Employee.objects.get(id=employee))
            Todo.save()
            return redirect("todolist")
        lista = []
        if "taskDelete" in request.POST:
            checkedlist = request.POST.getlist('checkedbox')
            for check in checkedlist:
                todo = TodoList.objects.get(id=check)
                todo.delete()

    return render(request, "todolist.html", {"todos": todos, "categories": categories, "employees": employees})


def employee_overview(request):
    # quering all todos with the object manager
    employees = Employee.objects.all()
    project = Projects.objects.all()
    context = {"employees": employees}

    if request.method == "POST":
        employee_id = request.POST["employee_id"]
        # print(employee_id)
        context["employee_id"] = employee_id
        employee_selected = Employee.objects.get(id=employee_id)
        context["employee_selected"] = employee_selected
        if "Search" in request.POST:
            todos = TodoList.objects.filter(employee_id=employee_id)
            # projects = Projects.objects.filter(employee_id=employee_id)
            context["todos"] = todos
            # context["projects"] = projects
            #
            # context["showproject"] = projects


        elif "Save" in request.POST:
            firstname = request.POST["firstname"]
            lastname = request.POST["lastname"]
            skill = request.POST["skill"]
            department = request.POST["department"]
            email = request.POST["email"]
            title = request.POST["title"]

            employee_selected.firstname = firstname
            employee_selected.lastname = lastname
            employee_selected.skill = skill
            employee_selected.department = department
            employee_selected.email = email
            employee_selected.title = title

            employee_selected.save()

        elif "taskDelete" in request.POST:
            checkedlist1 = request.POST.getlist('checkedbox1')
            for check in checkedlist1:
                todo = TodoList.objects.get(id=check)
                todo.delete()

            checkedlist2 = request.POST.getlist('checkedbox2')
            # print(checkedlist2)
            for project_id in checkedlist2:
                Projects.objects.filter(id=project_id).update(employee_id=None)

        projects = Projects.objects.filter(employee_id=employee_id)
        context["projects"] = projects



    return render(request, "employee_overview.html", context)


def exportToDoList(request):
    # category_resource = CategoryResource()
    todolist_resource = TodolistResource()
    # dataset1 = category_resource.export()
    dataset2 = todolist_resource.export()
    # response1 = HttpResponse(dataset1.xls, content_type='application/vnd.ms-excel')
    # response1['Content-Disposition'] = 'attachment; filename="category.xls"'
    response2 = HttpResponse(dataset2.xls, content_type='application/vnd.ms-excel')
    response2['Content-Disposition'] = 'attachment; filename="todolist.xls"'
    return response2



